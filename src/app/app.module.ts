import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PageMainModule} from './page-main/page-main.module';
import {HeaderModule} from './header/header.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, PageMainModule, HeaderModule,HttpClientModule],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
 