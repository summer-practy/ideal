import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainCategoriesComponent} from './main-categories.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [MainCategoriesComponent],
    imports: [CommonModule, RouterModule],
    exports: [MainCategoriesComponent]  
})
export class MainCategoriesModule {}

