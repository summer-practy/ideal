import {Component, OnInit} from '@angular/core';
import {BackendService} from '../../../services/backend.service';
import {Observable} from 'rxjs';
import {Category} from '../../../interfaces/category.interface';
import {NavigationService} from 'src/services/navigation.service';

@Component({
    selector: 'app-main-categories',
    templateUrl: './main-categories.component.html',
    styleUrls: ['./main-categories.component.css']
})
export class MainCategoriesComponent implements OnInit {
    categories$: Observable<Category[]> = this.backendService.categories$;

    constructor(
        private readonly backendService: BackendService,
        private readonly navigationService: NavigationService
    ) {}

    ngOnInit() {}
    toProducts(categoryId: string) {
        this.navigationService.toProducts(categoryId);
    }
}
