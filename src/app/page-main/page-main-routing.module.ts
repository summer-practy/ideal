import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AboutComponent} from './about/about.component';
import {MainCategoriesComponent} from './main-categories/main-categories.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = [
    {path: 'categories', component: MainCategoriesComponent},
    {path: 'about', component: AboutComponent},
    {path: 'categories/:categoryId/products', component: ProductsComponent},
    {path: 'products/:productId', component: ProductComponent},
    
    {path: '**', redirectTo: 'categories'}
  
];

@NgModule({imports: [RouterModule.forRoot(routes)], exports: [RouterModule]})
export class PageMainRoutingModule {}
