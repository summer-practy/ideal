import {Component, OnInit} from '@angular/core';
import {ExampleService} from 'src/services/exmple.service';

@Component({
    selector: 'app-page-main',
    templateUrl: './page-main.component.html',
    styleUrls: ['./page-main.component.css']
})
export class PageMainComponent implements OnInit {
    constructor(private readonly exampleService: ExampleService) {}
    get mainService(): string {
        return this.exampleService.mainService;
    }
    ngOnInit(): void {}
}
