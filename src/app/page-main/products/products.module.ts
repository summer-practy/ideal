import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductsComponent} from './products.component';
import { ActivatedRoute } from '@angular/router';

@NgModule({
    declarations: [ProductsComponent],
    imports: [CommonModule],
    exports: [ProductsComponent]
})
export class ProductsModule {
  constructor(private readonly route: ActivatedRoute){
    console.log (route.snapshot.paramMap.get('categoryId'))
  }
}
