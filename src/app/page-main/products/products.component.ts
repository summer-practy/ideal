import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {Product} from 'src/interfaces/product.interface';
import {BackendService} from 'src/services/backend.service';
import {NavigationService} from 'src/services/navigation.service';

@Component({
    selector: 'app-products',
    templateUrl: './products.template.html',
    styleUrls: ['./products.style.css']
})
export class ProductsComponent implements OnInit {
    readonly categoryId: string | null =
        this.activatedRoute.snapshot.paramMap.get('categoryId');

    readonly products$: Observable<Product[]> = this.backendService.getProducts(
        this.categoryId!
    );
    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly navigationService: NavigationService,
        private readonly backendService: BackendService
    ) {}

    ngOnInit(): void {}
    toItem(productId: string) {
        this.navigationService.toItem(productId);
    }
}
