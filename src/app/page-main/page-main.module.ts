import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageMainComponent} from './page-main.component';
import {MainCategoriesModule} from './main-categories/main-categories.module';
import {AboutModule} from './about/about.module';
import {PageMainRoutingModule} from './page-main-routing.module';
import {ProductsModule} from './products/products.module';
import {ProductModule} from './product/product.module';

@NgModule({
    declarations: [PageMainComponent],
    imports: [
        CommonModule,
        MainCategoriesModule,
        AboutModule,
        ProductsModule,
        ProductModule,
        PageMainRoutingModule
    ],
    exports: [PageMainComponent]
})
export class PageMainModule {}
