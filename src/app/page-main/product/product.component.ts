import {Component, Injectable, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {ProductInfo} from 'src/interfaces/product-info.interface';
import {BackendService} from 'src/services/backend.service';
import {CartService} from 'src/services/cart.service';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
    readonly productId: string | null =
        this.activatedRoute.snapshot.paramMap.get('productId');
    readonly product$: Observable<ProductInfo> = this.backendService.getProduct(
        this.productId!
    );

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly backendService: BackendService,
        private readonly cartService: CartService
    ) {}

    ngOnInit(): void {}
    addToCart(product: ProductInfo) {
        this.cartService.addItem(product);
    }
}
