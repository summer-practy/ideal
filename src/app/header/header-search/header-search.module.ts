import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderSearchComponent} from './header-search.component';
import {TuiComboBoxModule} from '@taiga-ui/kit';

@NgModule({
    declarations: [HeaderSearchComponent],
    imports: [CommonModule, TuiComboBoxModule],
    exports: [HeaderSearchComponent]
})
export class HeaderSearchModule {}
