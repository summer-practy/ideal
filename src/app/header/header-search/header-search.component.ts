import {Component} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {delay, filter, startWith, switchMap} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

class User {
    constructor(
        readonly firstName: string,
        readonly lastName: string,
        readonly avatarUrl: string | null = null
    ) {}

    toString(): string {
        return `${this.firstName} ${this.lastName}`;
    }
}

const databaseMockData: ReadonlyArray<User> = [
    new User('Roman', 'Sedov', 'http://marsibarsi.me/images/1x1small.jpg'),
    new User('Alex', 'Inkin', 'http://marsibarsi.me/images/1x1small.jpg')
];

@Component({
    selector: 'app-header-search',
    templateUrl: './header-search.component.html',
    styleUrls: ['./header-search.component.css']
})
export class HeaderSearchComponent {
    readonly search$ = new Subject<string>();

    readonly items$: Observable<ReadonlyArray<User> | null> = this.search$.pipe(
        filter(value => value !== null),
        switchMap(search =>
            this.serverRequest(search).pipe(startWith<ReadonlyArray<User> | null>(null))
        ),
        startWith(databaseMockData)
    );

    readonly testValue = new FormControl(null);

    onSearchChange(searchQuery: string) {
        this.search$.next(searchQuery);
    }

    /**
     * Service request emulation
     */
    private serverRequest(searchQuery: string): Observable<ReadonlyArray<User>> {
        const result = databaseMockData.filter(
            user =>
                user.toString().toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1
        );

        return of(result).pipe(delay(Math.random() * 1000 + 500));
    }
}
