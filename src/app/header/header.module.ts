import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header.component';

import {HeaderLogoModule} from './header-logo/header-logo.module';
import {HeaderSearchModule} from './header-search/header-search.module';
import {HeaderProfileModule} from './header-profile/header-profile.module';
import {HeaderCartModule} from './header-cart/header-cart.module';

@NgModule({
    declarations: [HeaderComponent],
    imports: [
        CommonModule,
        HeaderLogoModule,
        HeaderSearchModule,
        HeaderProfileModule,
        HeaderCartModule
    ],
    providers: [],
    exports: [HeaderComponent]
})
export class HeaderModule {}
