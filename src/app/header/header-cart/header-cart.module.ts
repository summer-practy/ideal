import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderCartComponent} from './header-cart.component';

@NgModule({
    declarations: [HeaderCartComponent],
    imports: [CommonModule],
    providers: [],
    exports: [HeaderCartComponent]
})
export class HeaderCartModule {}
