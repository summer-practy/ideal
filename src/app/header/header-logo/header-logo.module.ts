import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderLogoComponent} from './header-logo.component';

@NgModule({
    declarations: [HeaderLogoComponent],
    imports: [CommonModule],
    providers: [],
    exports: [HeaderLogoComponent]
})
export class HeaderLogoModule {}
