import {Component, OnInit} from '@angular/core';
import {ExampleService} from 'src/services/exmple.service';

@Component({
    selector: 'app-header-logo',
    templateUrl: './header-logo.component.html',
    styleUrls: ['./header-logo.component.css']
})
export class HeaderLogoComponent implements OnInit {
    constructor(readonly exampleService: ExampleService) {}

    ngOnInit(): void {}
}
