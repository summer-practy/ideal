import {Category, CategoryBm} from 'src/interfaces/category.interface';

export const categoryMapper = (categoryBm: CategoryBm): Category => ({
    id: categoryBm.category_id,
    name: categoryBm.category_name
});
