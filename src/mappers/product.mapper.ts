import { Product, ProductBm } from "src/interfaces/product.interface";

export const productMapper = (productBm: ProductBm): Product => ({
    id: productBm.product_id,
    name: productBm.product_name,
    price: productBm.product_price,
    imgUrl: productBm.product_image_url
});