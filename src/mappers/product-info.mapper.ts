import { ProductInfo, ProductInfoBm } from "src/interfaces/product-info.interface";

export const productInfoMapper = (productInfoBm: ProductInfoBm): ProductInfo => ({
    sizes: productInfoBm.size,
    description: productInfoBm.product_description,
    id: productInfoBm.product_id,
    name: productInfoBm.product_name,
    price: productInfoBm.product_price,
    imgUrl: productInfoBm.product_image_url
});