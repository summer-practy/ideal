import {Observable, of} from 'rxjs';
import {Category, CategoryBm} from '../interfaces/category.interface';
import {Injectable} from '@angular/core';
import {Product} from 'src/interfaces/product.interface';
import {stringHashToHsl} from '@taiga-ui/kit';
import {ProductInfo} from 'src/interfaces/product-info.interface';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { categoryMapper } from 'src/mappers/category.mapper';

@Injectable({providedIn: 'root'})
export class BackendService {
    categories$: Observable<Category[]> = this.http.get<CategoryBm[]>(
        'https://tfpractice.herokuapp.com/category'
    ).pipe(map(categories => categories.map(categoryMapper)));

    constructor(private http: HttpClient) {}

    getProduct(productsId: string): Observable<ProductInfo> {
        return of({
            id: '1',
            name: 'Vittoria Queen',
            price: 5700,
            sizes: [48, 50, 52],
            description: 'Красное платье в белый горошек. Состав: 100% ПЭ. Страна: Бел',
            imgUrl: 'https://sun1-83.userapi.com/impg/4b7Nyp-ZoMWywHifOEitK1KkQeRaCNoLt3YCmw/hZb8Chg-xHk.jpg?size=520x0&quality=95&sign=71d4d0016f746b619a1cfed2cd04019e'
        });
    }

    getProducts(categoryId: string): Observable<Product[]> {
        return of([
            {
                id: '1',
                name: 'Vittoria Queen',
                price: 5700,
                imgUrl: 'https://sun1-83.userapi.com/impg/4b7Nyp-ZoMWywHifOEitK1KkQeRaCNoLt3YCmw/hZb8Chg-xHk.jpg?size=520x0&quality=95&sign=71d4d0016f746b619a1cfed2cd04019e'
            },
            {
                id: '2',
                name: 'Mer Park',
                price: 2890,
                imgUrl: 'https://sun1-83.userapi.com/impg/4b7Nyp-ZoMWywHifOEitK1KkQeRaCNoLt3YCmw/hZb8Chg-xHk.jpg?size=520x0&quality=95&sign=71d4d0016f746b619a1cfed2cd04019e'
            },
            {
                id: '1',
                name: 'Vittoria Queen',
                price: 5700,
                imgUrl: 'https://sun1-83.userapi.com/impg/4b7Nyp-ZoMWywHifOEitK1KkQeRaCNoLt3YCmw/hZb8Chg-xHk.jpg?size=520x0&quality=95&sign=71d4d0016f746b619a1cfed2cd04019e'
            },
            {
                id: '2',
                name: 'Mer Park',
                price: 2890,
                imgUrl: 'https://sun1-83.userapi.com/impg/4b7Nyp-ZoMWywHifOEitK1KkQeRaCNoLt3YCmw/hZb8Chg-xHk.jpg?size=520x0&quality=95&sign=71d4d0016f746b619a1cfed2cd04019e'
            },
            {
                id: '1',
                name: 'Vittoria Queen',
                price: 5700,
                imgUrl: 'https://sun1-83.userapi.com/impg/4b7Nyp-ZoMWywHifOEitK1KkQeRaCNoLt3YCmw/hZb8Chg-xHk.jpg?size=520x0&quality=95&sign=71d4d0016f746b619a1cfed2cd04019e'
            },
            {
                id: '2',
                name: 'Mer Park',
                price: 2890,
                imgUrl: 'https://sun1-83.userapi.com/impg/4b7Nyp-ZoMWywHifOEitK1KkQeRaCNoLt3YCmw/hZb8Chg-xHk.jpg?size=520x0&quality=95&sign=71d4d0016f746b619a1cfed2cd04019e'
            }
        ]);
    }
}
