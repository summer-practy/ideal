import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({providedIn: 'root'})
export class NavigationService {
    constructor(private readonly router: Router) {}

    toProducts(categoryId: string) {
        this.router.navigate([`/categories/${categoryId}/products`]);
    }
    toItem(productId: string){
        this.router.navigate([`/products/${productId}`]);
    }

}
