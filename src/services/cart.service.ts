import {Injectable} from '@angular/core';
import {ProductInfo} from 'src/interfaces/product-info.interface';

@Injectable({
    providedIn: 'root'
})
export class CartService {
    items: ProductInfo[] = [];
    addItem(item: ProductInfo) {
        console.log(this.items);
        this.items = [...this.items, item];
        console.log(this.items);
    }
    removeItem(index: number) {
        this.items = this.items.filter((_, i) => i !== index);
    }
}
