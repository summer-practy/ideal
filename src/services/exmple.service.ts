import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ExampleService {
    mainService = 'мама мыла раму';
    constructor() {}
    onMainServiceChange() {
        this.mainService = this.mainService + ' папа мыл машину';
    }
}
