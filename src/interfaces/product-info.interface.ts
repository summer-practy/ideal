import { Product, ProductBm } from "./product.interface";

export interface ProductInfo extends Product {
    sizes: number[];
    description: string;


    
}
export interface ProductInfoBm extends ProductBm {

    size:number[];
    product_description: string;
    product_price: number;
    product_image_url: string
} 

    
