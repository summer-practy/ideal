export interface Category {
    id: string;
    name: string;
}
export interface CategoryBm {
    category_id: string;
    category_name: string;
}
