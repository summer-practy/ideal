export interface Product {
    id: string;
    name: string;
    price: number;
    imgUrl:string;
}
export interface ProductBm {
    product_id: string;
    product_name: string;
    product_price: number;
    product_image_url: string
}